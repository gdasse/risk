class territoire{
    constructor(nomTerritoire, coordonnes, continent){
        this.nomTerritoire = nomTerritoire;
        this.coordonnes = coordonnes;
        this._nombreUnite;
        this._Proprietaire;
        this.territoiresAdjacents = [];
        this.text;
        this.continent = continent
        this.center
        this.maxDistance
        this.marker
    }

    get NomTerritoire(){
        return this.nomTerritoire;
    }

    get Coordonnes(){
        return this.coordonnes;
    }

    get NombreUnite(){
        return this._nombreUnite;
    }

    set NombreUnite(nombreUnite){
        this._nombreUnite = nombreUnite;
    }

    get Joueur(){
        return this._Proprietaire;
    }

    set Joueur(proprietaire){
        this._Proprietaire = proprietaire;
    }

    set Center(center){
        this.center = center
        this.définirMaxDistance()
    }

    définirMaxDistance(){
        var min
        var center = this.center
        this.Coordonnes.forEach(function(element) {
            var resultat = Math.sqrt(Math.pow(element[0] - center.lat,2) + Math.pow(element[1] - center.lng,2))
            if(min == undefined || min > resultat)
                min = resultat 
        })
        this.maxDistance = min/2
    }

    estAdjacent(territoire){
        return this.territoiresAdjacents.includes(territoire);
    }

    ajouterTerritoireAdj(territoire){
        this.territoiresAdjacents.push(territoire);
    }

    afficherSoldats(map){
        if(this.marker!=undefined)
            this.marker.remove()
        this.marker = new L.MarkerClusterGroup();
        for(var i = 0; i < this.NombreUnite; i++){
            var lat = this.center.lat
            var lng = this.center.lng
            this.marker.addLayer(L.marker([lat, lng]))
        }
        map.addLayer(this.marker);
    }
}