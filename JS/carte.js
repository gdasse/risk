/**
 * Gauthier DASSE 22/03/2020
 */

var map = L.map('map', {
    minZoom: 2,
    maxZoom: 2
});

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    noWrap: true
}).addTo(map)

map.setView([50, 24], 2)

$("#button__afficherRegle").click(function () {
    $("#head").css("display", "none");
    $("#fieldJeu").css("display", "none");
    $("#regle").css("display", "block");
})

$("#button__cacherRegle").click(function () {
    $("#regle").css("display", "none");
    $("#head").css("display", "flex");
    $("#fieldJeu").css("display", "block");
})

//liste des joueurs classés en fonction de leur nom
var listeJoueurs = []
//index du joueur qui joue
var JoueurActuel
//liste des territoires placé en fonction de l'ID du polygon
var listeTerritoires = []
var couleurs = ['red', 'blue', 'orange', 'green', 'purple', 'pink']
//territoire qui attaque
var polygonAttaquant
//territoire qui défent
var polygonDefenseur
//initialisation du numéro de phase (2 car l'appel changerTour va le mettre à 0)
var phase = 2
var nomPhase = ["deploiement", ["l'attaque"], ["Déplacement"]]
//nombre de soldats qui pourront etre ajouté lors du déploiement
var nombreSoldatsAjouter
//contient le territoire sur lequel la souris est
var territoireSurvole
//timer pour afficher les informations du pays
var movementTimer
//liste contenants les continents (qui contiennent la liste des territoires associés)
var continents = []
//liste des bonus associés au continant relié par l'index dans le tableau des continents
var bonusSoldats = []

/**
 * Initialise la partie
 */
function initialisationCarte() {

    var territoires = []
    //ensemble des continents avec les bonus
    var AmeriqueDuNord = []
    var AmeriqueDuSud = []
    var Europe = []
    var Afrique = []
    var Asie = []
    var Oceanie = []
    var MoyenOrient = []
    continents.push(AmeriqueDuNord)
    continents.push(AmeriqueDuSud)
    continents.push(Europe)
    continents.push(Afrique)
    continents.push(Asie)
    continents.push(Oceanie)
    continents.push(MoyenOrient)
    bonusSoldats.push(5)//bonus amérique du Nord
    bonusSoldats.push(2)//bonus amérique du Sud
    bonusSoldats.push(6)
    bonusSoldats.push(4)
    bonusSoldats.push(6)
    bonusSoldats.push(2)
    bonusSoldats.push(3)

    //création de tous les territoires
    var Canada = new territoire('Canada', [
        [59.7121, -139.92188], [48.57479, -123.39844]
        , [46.92026, -53.08594], [67.06743, -61.875], [68.0733, -68.55469]
        , [69.47297, -140.27344], [59.7121, -139.92188]
    ], "Amérique du Nord")
    //ajout du nouveau territoire dans le jeu
    territoires.push(Canada)
    //affectation du territoire à son continent
    AmeriqueDuNord.push(Canada)
    

    var Glaçons = new territoire('Glaçons', [
        [69.47297, -140.27344], [77.19618, -119.17969], [83.27771, -77.34375],
        [82.76537, -63.45703], [82.37915, -60.11719], [79.93592, -70.48828],
        [74.72962, -78.22266], [67.06743, -61.875], [68.0733, -68.55469],
        [69.47297, -140.27344]
    ], "Amérique du Nord")
    territoires.push(Glaçons)
    //définit que Canada et Glaçons sont à côté
    ajouterTerritoireAdj(Canada, Glaçons)
    AmeriqueDuNord.push(Glaçons)

    var Groenloand = new territoire("Groendland", [
        [82.37915, -60.11719], [79.93592, -70.48828], [74.72962, -78.22266], [67.06743, -61.875],
        [58.63122, -44.38477], [69.96044, -21.00586], [81.34808, -10.37109], [83.94227, -29.17969],
        [82.37915, -60.11719]
    ], "Amérique du Nord")
    territoires.push(Groenloand)
    ajouterTerritoireAdj(Groenloand, Glaçons)
    AmeriqueDuNord.push(Groenloand)

    var Islande = new territoire("Islande", [
        [67.30598, -18.80859], [65.73063, -25.04883], [63.39152, -24.16992],
        [63.35213, -15.0293], [64.88627, -11.95313], [67.30598, -18.80859]
    ], "Europe")
    territoires.push(Islande)
    Europe.push(Islande)
    ajouterTerritoireAdj(Groenloand, Islande)

    var Alaska = new territoire('Alaska', [
        [69.47297, -140.27344], [59.7121, -139.92188],
        [59.3556, -164.70703], [70.84467, -165.41016]
    ], "Amérique du Nord")
    territoires.push(Alaska)
    ajouterTerritoireAdj(Canada, Alaska)
    AmeriqueDuNord.push(Alaska)

    var Etat_Unis = new territoire('Etat-Unis', [
        [48.57479, -123.39844], [46.92026, -53.08594],
        [25.48295, -81.03516], [30.50363, -120.07031],
        [49.57479, -123.39844]
    ], "Amérique du Nord")

    ajouterTerritoireAdj(Canada, Etat_Unis)
    territoires.push(Etat_Unis)
    AmeriqueDuNord.push(Etat_Unis)

    var AmeriqueCentrale = new territoire('Amérique Centrale', [
        [25.48295, -81.03516], [30.50363, -120.07031],
        [6.83917, -90.87891], [11.86735, -59.94141], [25.48295, -81.03516]
    ], "Amérique du Nord")
    territoires.push(AmeriqueCentrale)
    ajouterTerritoireAdj(AmeriqueCentrale, Etat_Unis)
    AmeriqueDuNord.push(AmeriqueCentrale)

    var Colombie = new territoire('Colombie', [
        [6.83917, -90.87891], [11.86735, -59.94141],
        [2.46018, -51.85547], [-4.21494, -80.85938]
    ], "Amérique du Sud")
    territoires.push(Colombie)
    AmeriqueDuSud.push(Colombie)
    ajouterTerritoireAdj(AmeriqueCentrale, Colombie)

    var Bresil = new territoire('Brésil', [
        [2.46018, -51.85547], [-4.21494, -80.85938],
        [-18.81272, -70.3125], [-33.13755, -52.03125],
        [-5.96575, -34.45313], [2.46018, -51.85547]
    ], "Amérique du Sud")
    territoires.push(Bresil)
    AmeriqueDuSud.push(Bresil)
    ajouterTerritoireAdj(Bresil, Colombie)

    var Argentine = new territoire('Argentine', [
        [-18.81272, -70.3125], [-33.13755, -52.03125],
        [-55.07837, -66.97266], [-52.3756, -75.23438],
        [-18.81272, -70.3125]
    ], "Amérique du Sud")
    territoires.push(Argentine)
    AmeriqueDuSud.push(Argentine)
    ajouterTerritoireAdj(Bresil, Argentine)

    var EuropeOuest = new territoire("Europe de l'Ouest", [
        [37.16032, -8.96484], [48.57479, -5.27344], [57.13624, 9.84375],
        [50.62507, 15.99609], [46.55886, 10.37109], [43.83453, 7.38281],
        [36.73888, -1.58203], [37.16032, -8.96484]
    ], "Europe")
    territoires.push(EuropeOuest)
    Europe.push(EuropeOuest)

    var EuropdeSud = new territoire("Europe du Sud", [
        [50.62507, 15.99609], [46.55886, 10.37109], [43.83453, 7.38281],
        [39.23225, 8.26172], [35.88905, 14.76563], [36.31513, 23.55469],
        [41.37681, 27.59766], [45.213, 30.32227], [50.62507, 15.99609]
    ], "Europe")
    territoires.push(EuropdeSud)
    Europe.push(EuropdeSud)
    ajouterTerritoireAdj(EuropeOuest, EuropdeSud)

    var EuropedeEst = new territoire("Europe de l'Est", [
        [53.80065, 14.32617], [50.62507, 15.99609], [45.213, 30.32227],
        [47.5172, 39.02344], [49.43956, 39.72656], [52.26816, 31.72852], [55.52863, 30.76172],
        [59.57885, 28.21289], [58.81374, 21.62109], [54.87661, 19.51172], [53.80065, 14.32617]
    ], "Europe")
    territoires.push(EuropedeEst)
    Europe.push(EuropedeEst)
    ajouterTerritoireAdj(EuropeOuest, EuropedeEst)
    ajouterTerritoireAdj(EuropdeSud, EuropedeEst)


    var Russie = new territoire("Russie", [
        [47.5172, 39.02344], [49.43956, 39.72656], [52.26816, 31.72852],
        [55.52863, 30.76172], [59.57885, 28.21289], [62.79493, 31.81641],
        [68.72044, 28.74023], [69.77895, 31.11328], [67.87554, 40.60547], [75.62863, 55.54688],
        [76.76054, 68.37891], [77.69287, 103.88672], [76.31036, 142.38281],
        [71.35707, 182.63672], [66.16051, 191.07422],
        [50.73646, 157.32422], [49.61071, 87.1875], [55.12865, 69.60938],
        [48.57479, 46.93359], [41.37681, 47.98828], [41.37681, 47.98828],
        [43.32518, 38.49609], [47.5172, 39.02344]
    ], "Asie")
    territoires.push(Russie)
    Asie.push(Russie)
    ajouterTerritoireAdj(Russie, EuropedeEst)
    ajouterTerritoireAdj(Russie, Alaska)

    var RoyaumeUnis = new territoire("Royaume-Unis", [
        [48.57479, -5.27344], [57.13624, 9.84375], [58.44773, -2.98828],
        [51.61802, -10.37109], [48.57479, -5.27344]
    ], "Europe")
    territoires.push(RoyaumeUnis)
    Europe.push(RoyaumeUnis)
    ajouterTerritoireAdj(RoyaumeUnis, EuropeOuest)
    ajouterTerritoireAdj(RoyaumeUnis, Islande)

    var PaysScandinaves = new territoire("Pays Scandinaves", [
        [57.13624, 9.84375], [57.91485, 6.50391], [61.54364, 3.60352],
        [67.70945, 11.46973], [71.35707, 25.04883], [69.77895, 31.11328],
        [68.72044, 28.74023], [62.79493, 31.81641], [59.57885, 28.21289],
        [58.81374, 21.62109], [54.87661, 19.51172], [53.80065, 14.32617]
    ], "Europe")
    territoires.push(PaysScandinaves)
    Europe.push(PaysScandinaves)
    ajouterTerritoireAdj(RoyaumeUnis, PaysScandinaves)
    ajouterTerritoireAdj(EuropeOuest, PaysScandinaves)
    ajouterTerritoireAdj(EuropedeEst, PaysScandinaves)
    ajouterTerritoireAdj(Russie, PaysScandinaves)
    ajouterTerritoireAdj(Islande, PaysScandinaves)

    var AfriqueOuest = new territoire("Afrique de L'Ouest", [
        [37.16032, -8.96484], [20.63278, -17.57813], [12.55456, -17.40234],
        [4.03962, -8.4375], [3.68886, 8.61328], [22.26876, 14.58984], [32.39852, 11.60156],
        [36.45664, 10.54688], [37.16032, -8.96484]
    ], "Afrique")
    territoires.push(AfriqueOuest)
    Afrique.push(AfriqueOuest)
    ajouterTerritoireAdj(EuropeOuest, AfriqueOuest)
    ajouterTerritoireAdj(EuropdeSud, AfriqueOuest)
    ajouterTerritoireAdj(Bresil, AfriqueOuest)

    var AfriqueNord = new territoire("Nord de l'Afrique", [
        [3.68886, 8.61328], [22.26876, 14.58984], [32.39852, 11.60156], [31.65338, 25.3125],
        [21.77991, 25.48828], [8.58102, 24.43359], [1.93323, 16.17188], [3.68886, 8.61328]
    ], "Afrique")
    territoires.push(AfriqueNord)
    Afrique.push(AfriqueNord)
    ajouterTerritoireAdj(AfriqueNord, AfriqueOuest)

    var Egypte = new territoire("Egypte", [
        [31.65338, 25.3125], [21.77991, 25.48828], [22.106, 37.26563], [29.993, 35.15625], [31.65338, 25.3125]
    ], "Afrique")
    territoires.push(Egypte)
    Afrique.push(Egypte)
    ajouterTerritoireAdj(AfriqueNord, Egypte)

    var ArabieSaoudite = new territoire("Arabie Saoudite", [
        [29.993, 35.15625], [31.50363, 34.45313], [35.88905, 36.5625], [36.45664, 45.35156],
        [29.84064, 49.21875], [22.91792, 60.29297], [16.46769, 53.78906], [13.06878, 43.76953],
        [29.993, 35.15625]
    ], "Moyen-Orient")
    territoires.push(ArabieSaoudite)
    MoyenOrient.push(ArabieSaoudite)
    ajouterTerritoireAdj(ArabieSaoudite, Egypte)

    var Iran = new territoire("Iran", [
        [29.84064, 49.21875], [37.0201, 44.56055], [38.47939, 61.43555],
        [25.0856, 62.8418], [22.91792, 60.29297]
    ], "Moyen-Orient")
    territoires.push(Iran)
    MoyenOrient.push(Iran)
    ajouterTerritoireAdj(ArabieSaoudite, Iran)

    var Pakistan = new territoire("Pakistan", [
        [38.47939, 61.43555], [37.16032, 75.32227], [24.44715, 70.92773], [25.0856, 62.8418]
    ], "Asie")
    territoires.push(Pakistan)
    Asie.push(Pakistan)
    ajouterTerritoireAdj(Pakistan, Iran)

    var Inde = new territoire("Inde", [
        [37.16032, 75.32227], [26.11599, 89.38477], [22.02455, 92.02148], [6.05316, 82.88086], [6.14055, 72.94922],
        [22.02455, 68.55469], [24.44715, 70.92773], [37.16032, 75.32227]
    ], "Asie")
    territoires.push(Inde)
    Asie.push(Inde)
    ajouterTerritoireAdj(Pakistan, Inde)
    ajouterTerritoireAdj(Russie, Inde)
    ajouterTerritoireAdj(Iran, Inde)

    var Kazakhstan = new territoire("Kazakhstan", [
        [38.47939, 53.70117], [48.57479, 46.93359], [55.12865, 69.60938],
        [49.21042, 87.53906], [37.16032, 75.32227], [38.47939, 53.70117]
    ], "Asie")
    territoires.push(Kazakhstan)
    Asie.push(Kazakhstan)
    ajouterTerritoireAdj(Kazakhstan, Inde)
    ajouterTerritoireAdj(Iran, Kazakhstan)
    ajouterTerritoireAdj(Russie, Kazakhstan)

    var Chine = new territoire("Chine", [
        [49.21042, 87.53906], [37.16032, 75.32227], [26.11599, 89.38477], [22.02455, 92.02148],
        [20.46819, 118.82813], [34.59704, 129.02344], [47.15984, 138.16406], [50.1769, 141.50391]
    ], "Asie")
    territoires.push(Chine)
    Asie.push(Chine)
    ajouterTerritoireAdj(Russie, Chine)
    ajouterTerritoireAdj(Chine, Inde)
    ajouterTerritoireAdj(Kazakhstan, Chine)

    var Japon = new territoire("Japon", [
        [47.15984, 138.16406], [33.54139, 128.250],
        [30.44867, 130.95703], [33.72434, 142.55859], [43.70759, 148.00781],
        [50.1769, 141.50391]
    ], "Asie")
    territoires.push(Japon)
    Asie.push(Japon)
    ajouterTerritoireAdj(Japon, Chine)
    ajouterTerritoireAdj(Japon, Russie)

    var Cambodge = new territoire("Cambodge", [
        [22.02455, 92.02148], [20.46819, 118.82813], [8.40717, 110.91797],
        [6.83917, 94.92188], [22.02455, 92.02148]
    ], "Oceanie")
    territoires.push(Cambodge)
    Oceanie.push(Cambodge)
    ajouterTerritoireAdj(Cambodge, Chine)
    ajouterTerritoireAdj(Cambodge, Inde)

    var OceanieOuest = new territoire("Océanie de l'Ouest", [
        [6.83917, 94.92188], [8.40717, 110.91797], [20.46819, 118.82813],
        [17.64402, 126.91406], [-9.44906, 127.96875], [-10.14193, 105.64453]
    ], "Oceanie")
    territoires.push(OceanieOuest)
    Oceanie.push(OceanieOuest)
    ajouterTerritoireAdj(Cambodge, OceanieOuest)

    var OceanieEst = new territoire("Océanie de l'Est", [
        [-9.44906, 127.96875], [-10.66061, 131.66016], [-11.0059, 142.20703], [-11.3508, 163.125],
        [-1.23037, 156.09375], [4.56547, 128.14453]
    ], "Oceanie")
    territoires.push(OceanieEst)
    Oceanie.push(OceanieEst)
    ajouterTerritoireAdj(OceanieEst, OceanieOuest)

    var Autralie = new territoire("Australie", [
        [-10.66061, 131.66016], [-22.43134, 113.20313], [-36.17336, 112.85156],
        [-33.57801, 131.13281], [-44.46515, 145.89844], [-29.53523, 155.39063],
        [-11.0059, 142.20703], [-10.66061, 131.66016]
    ], "Oceanie")
    territoires.push(Autralie)
    Oceanie.push(Autralie)
    ajouterTerritoireAdj(OceanieEst, Autralie)


    var Turquie = new territoire("Turquie", [
        [41.64008, 28.38867], [39.70719, 25.57617], [36.24427, 27.42188],
        [36.24427, 36.91406], [37.0201, 44.64844], [37.37016, 49.57031],
        [41.44273, 48.95508], [43.64403, 39.375], [41.64008, 28.38867]
    ], "Moyen-Orient")
    territoires.push(Turquie)
    MoyenOrient.push(Turquie)
    ajouterTerritoireAdj(Turquie, EuropdeSud)
    ajouterTerritoireAdj(Turquie, Russie)
    ajouterTerritoireAdj(Turquie, ArabieSaoudite)
    ajouterTerritoireAdj(Turquie, Iran)

    var AfriqueDuSud = new territoire("Afrique du Sud", [
        [-34.45222, 21.97266], [-34.16182, 28.47656], [-27.05913, 33.57422], [-22.43134, 31.99219],
        [-25.64153, 21.09375], [-28.45903, 16.52344], [-35.31737, 18.98438], [-34.45222, 21.97266]
    ], "Afrique")
    territoires.push(AfriqueDuSud)
    Afrique.push(AfriqueDuSud)

    var AfriqueDeEst = new territoire("Afrique de L'Est", [
        [21.77991, 37.44141], [12.55456, 44.12109], [11.69527, 51.32813],
        [-1.75754, 41.48438], [3.51342, 30.76172], [8.58102, 24.43359],
        [21.45307, 25.66406], [21.77991, 37.44141]
    ], "Afrique")
    territoires.push(AfriqueDeEst)
    Afrique.push(AfriqueDuSud)
    ajouterTerritoireAdj(AfriqueNord, AfriqueDeEst)
    ajouterTerritoireAdj(Egypte, AfriqueDeEst)
    ajouterTerritoireAdj(ArabieSaoudite, AfriqueDeEst)

    var Congo = new territoire("Congo", [
        [3.86425, 8.4375], [-17.14079, 11.42578], [-28.45903, 16.52344], [-25.64153, 21.09375],
        [-22.43134, 31.99219], [-27.05913, 33.57422], [-16.46769, 40.42969],
        [-2.63579, 41.66016], [8.23324, 24.43359], [1.93323, 16.17188], [3.86425, 8.4375]
    ], "Afrique")
    territoires.push(Congo)
    Afrique.push(AfriqueDuSud)
    ajouterTerritoireAdj(Congo, AfriqueDuSud)
    ajouterTerritoireAdj(Congo, AfriqueDeEst)
    ajouterTerritoireAdj(Congo, AfriqueNord)

    var Madagascar = new territoire("Madagascar", [
        [-24.68695, 42.36328], [-26.11599, 47.28516], [-13.23995, 51.32813],
        [-12.72608, 48.86719], [-16.97274, 43.59375], [-24.68695, 42.36328]
    ], "Afrique")
    territoires.push(Madagascar)
    Afrique.push(AfriqueDuSud)
    ajouterTerritoireAdj(Congo, Madagascar)



    //attribution des territoires entre les joueurs
    initialisationTerritoires(territoires)
    territoires.forEach(function (element) {
        //affiche les territoires avec la couleur de leur proprietaire
        var polygon = L.polygon(element.Coordonnes,
            { color: element.Joueur.Couleur, weight: 6, fillOpacity: 0.3 })
            .addTo(map);
        listeTerritoires[polygon._leaflet_id] = element
        //définit le nombre d'unité sur le territoire
        element.NombreUnite = 5

        element.Center = polygon.getBounds().getCenter()
        element.afficherSoldats(map)

        //définit les actions d'un click sur un territoire
        $(polygon).click(gererTerritoire)

        $(polygon).mouseenter(function () {
            territoireSurvole = getTerritoire(this);
        })

        $(polygon).mouseleave(function () {
            territoireSurvole = null
        })
    })


    //définit qui est le premier joueur
    JoueurActuel = Math.floor(Math.random() * nombreJoueur())
    changerTour()

    $("#risk__finTour").click(changerTour)

    //affiche les informations du territoire survolé au bout d'un certain temps
    $(document).mousemove(function (event) {
        clearTimeout(movementTimer);
        $("#risk__information").css("visibility", "hidden");
        movementTimer = setTimeout(function () {
            afficherInformation(territoireSurvole, event)

        }, 300);

    })
}

/**
 * Change le joueur qui joue
 */
function changerTour() {
    phase += 1
    if (phase == 3) {
        phase = 0
        JoueurActuel += 1
        if (JoueurActuel >= nombreJoueur())
            JoueurActuel = 0
    }
    polygonAttaquant = undefined
    afficherConsole("clear")
    $('#risk__finTour').text('Fin de ' + nomPhase[phase])
    $("#risk__joueurActuel").text("Tour de " + getJoueur(JoueurActuel).nomJoueur + " phase : " + nomPhase[phase])
    $("#risk__joueurActuel").css('color', getJoueur(JoueurActuel).Couleur)
    if (phase == 0) {
        //détermine le nombre de soldat à ajouter
        $("#risk__territoireSelectionner").text("")
        var nombreTerritoire = getNombreTerritoire(getJoueur(JoueurActuel))
        nombreSoldatsAjouter = Math.floor(nombreTerritoire / 3)
        //un minimum de 3 soldats à ajouter
        if (nombreSoldatsAjouter < 3)
            nombreSoldatsAjouter = 3
        nombreSoldatsAjouter += bonusContinents()
        afficherConsole("clear")
        afficherConsole("<p>Nombre de soldats à rajouter :" + nombreSoldatsAjouter + "</p>")
    } else {
        afficherTerritoireSelc();
    }
}

/**
 * Donne un joueur choisi aléatoirement
 */
function JoueurAléatoire() {
    var nombre = Math.floor(Math.random() * (nombreJoueur()))
    return getJoueur(nombre)
}

/**
 * Créer un nouveau joueur et l'ajoute à la liste des joueurs
 * Renvoie true si ajouter, false sinon
 * @param {String} nomJoueur le nom du joueur ajouté 
 */
function nouveauJoueur(nomJoueur) {
    if (listeJoueurs[nomJoueur] == undefined) {
        var couleur = couleurs[nombreJoueur()]
        listeJoueurs[nomJoueur] = new Joueur(couleur, nomJoueur)
        return true
    } else {
        return false
    }
}

/**
 * Donne le nombre de joueurs inscrit dans la partie
 */
function nombreJoueur() {
    return Object.keys(listeJoueurs).length
}

/**
 * Gère les règles de l'attaque
 */
function gererTerritoire() {

    if (phase == 0) {
        ajouterSoldat(this)
    } else {
        if (phase == 1) {
            attaquer(this)
        } else {
            déplacerSoldats(this)
        }
        afficherTerritoireSelc();
    }
}

/**
 * s'occupe des territoires dans la phase 0
 * @param {polygon} polygon 
 */
function ajouterSoldat(polygon) {
    var territoireSelect = getTerritoire(polygon)
    if (territoireSelect.Joueur.equals(getJoueur(JoueurActuel))) {
        if (nombreSoldatsAjouter == 0)
            afficherConsole("<p>Vous n'avez plus de soldats à rajouter</p>")
        else {
            nombreSoldatsAjouter--;
            territoireSelect.NombreUnite += 1;
            territoireSelect.afficherSoldats(map)
            afficherConsole("clear")
            afficherConsole("<p>Nombre de soldats à rajouter :" + nombreSoldatsAjouter + "</p>")
        }
    } else {
        afficherConsole("<p>Selectionner votre territoire</p>")
    }
}

/**
 * s'occupe des territoires dans la phase 2
 * @param {polygon} polygon 
 */
function déplacerSoldats(polygon) {
    var territoireSelect = getTerritoire(polygon);
    if (territoireSelect.Joueur.equals(getJoueur(JoueurActuel))) {
        if (polygonAttaquant == undefined) {
            polygonAttaquant = polygon
        } else {
            if (territoireSelect.estAdjacent(getTerritoire(polygonAttaquant))) {
                var territoireAttaquant = getTerritoire(polygonAttaquant)
                if (territoireAttaquant.NombreUnite > 1) {
                    territoireAttaquant.NombreUnite -= 1
                    territoireAttaquant.afficherSoldats(map)
                    territoireSelect.NombreUnite += 1
                    territoireSelect.afficherSoldats(map)
                    afficherConsole("clear")
                    afficherConsole("<p>" + territoireAttaquant.NomTerritoire + " a fourni 1 soldat au territoire " + territoireSelect.NomTerritoire + "</p>")
                } else {
                    afficherConsole("clear")
                    afficherConsole("<p> " + territoireAttaquant.NomTerritoire + " ne peut plus fournir de soldats</p>")
                }
            } else {
                if (territoireSelect.NomTerritoire === getTerritoire(polygonAttaquant).NomTerritoire) {
                    polygonAttaquant = null
                } else {
                    polygonAttaquant = polygon
                }
            }
        }
    } else {
        afficherConsole("<p>Selectionner votre territoire</p>")
    }
}

/**
 * s'occupe des territoires dans la phase 1
 * @param {polygon} polygon territoire
 */
function attaquer(polygon) {
    var territoireSelect = getTerritoire(polygon)
    afficherConsole("clear")
    if (territoireSelect.Joueur.equals(getJoueur(JoueurActuel))) {
        polygonAttaquant = polygon
        polygonDefenseur = undefined
    } else {
        if (polygonAttaquant == undefined) {
            afficherConsole("<p>Il faut séletionner un territoire attaquant</p>")
        } else {
            if (!territoireSelect.estAdjacent(getTerritoire(polygonAttaquant))) {
                afficherConsole("<p>Les territoires ne sont pas adjacents</p>")
            } else {
                if (polygonDefenseur == undefined || polygonDefenseur != polygon) {
                    polygonDefenseur = polygon
                    afficherConsole("<p>Recliquer sur ce territoire pour valider l'attaque</p>")
                } else {
                    combat(polygonAttaquant, polygonDefenseur)
                }
            }
        }
    }
}

/**
 * simule le combat entre deux territoires
 * @param {Leaflet} attaquant 
 * @param {Leaflet} defenseur 
 */
function combat(attaquant, defenseur) {
    afficherConsole('clear')
    var territoireAttaquant = getTerritoire(attaquant)
    var territoireDefenseur = getTerritoire(defenseur)
    afficherConsole("<p>" + territoireDefenseur.NomTerritoire + " est attaqué par " + territoireAttaquant.NomTerritoire + "</p>")
    var nombreSoldatAttaquant = territoireAttaquant.NombreUnite - 1
    var nombreSoldatDefenseur = territoireDefenseur.NombreUnite
    while (nombreSoldatAttaquant > 0 && nombreSoldatDefenseur > 0) {
        var desAttaquant = []
        var desDefenseur = []

        //lancer de dées des attaquants et des défenseurs
        for (var i = 0; i < nombreSoldatAttaquant && i < 3; i++) {
            desAttaquant.push(lancerDe())
        }
        desAttaquant.sort()
        desAttaquant.reverse()
        for (var i = 0; i < nombreSoldatDefenseur && i < 2; i++) {
            desDefenseur.push(lancerDe())
        }
        desDefenseur.sort();
        desDefenseur.reverse();

        //face à face
        var perteAttaquant = 0
        var perteDefenseur = 0
        for (var i = 0; i < desAttaquant.length && i < desDefenseur.length; i++) {
            if (desAttaquant[i] > desDefenseur[i])
                perteDefenseur += 1;
            else
                perteAttaquant += 1;
        }

        nombreSoldatAttaquant -= perteAttaquant
        nombreSoldatDefenseur -= perteDefenseur
        afficherConsole("clear")
        var message = "<p>Les attaquants ont perdu " + perteAttaquant + " soldats</p>"
            + "<p>Les défenseurs ont perdu " + perteDefenseur + " soldats</p>"
            + "<p>Il reste donc " + nombreSoldatAttaquant + " soldats attaquants"
            + " et " + nombreSoldatDefenseur + " soldats défendant</p>"
        afficherConsole(message)
    }

    //si les défenseurs ont perdu
    if (nombreSoldatDefenseur == 0) {
        afficherConsole("<p>Les défenseurs ont perdu </p>")
        var JoueurAttaquant = getJoueur(JoueurActuel)
        var JoueurDefenseur = territoireDefenseur.Joueur
        //changement de proprietaire
        territoireDefenseur.Joueur = JoueurAttaquant
        territoireDefenseur.NombreUnite = nombreSoldatAttaquant
        territoireDefenseur.afficherSoldats(map)
        defenseur.setStyle({ color: JoueurAttaquant.Couleur })
        territoireAttaquant.NombreUnite = 1
        territoireAttaquant.afficherSoldats(map)

        //si dernier territoire du defenseur
        if (getNombreTerritoire(JoueurDefenseur) == 0) {
            supprimerJoueur(JoueurDefenseur)
        }
    } else {
        afficherConsole("<p>Les attaquants ont perdu</p>")
        territoireDefenseur.NombreUnite = nombreSoldatDefenseur
        territoireDefenseur.afficherSoldats(map)
        territoireAttaquant.NombreUnite = nombreSoldatAttaquant + 1
        territoireAttaquant.afficherSoldats(map)
    }
}

/**
 * simule le lancer de dé à 6 faces
 */
function lancerDe() {

    var random;

    $.ajax({
        type: "GET",
        url: "https://www.random.org/integers/",
        data: "num=1&min=1&max=6&col=1&base=10&format=plain&rnd=new",
        dataType: "json",
        async: false,
        success: function (result) {
            random = result;
        },
        error: function () {
            random = Math.floor((Math.random() * 6) + 1);
        }
    });
    return random;
}

/**
 * Donne un joueur en fonction de son ordre d'inscription
 * @param {Integer} index 
 */
function getJoueur(index) {
    return listeJoueurs[Object.keys(listeJoueurs)[index]]
}

function getNombreJoueur() {
    return Object.keys(listeJoueurs).length
}


/**
 * définit les deux territoires commme adjacents
 * @param {territoire} territoire1 
 * @param {territoire} territoire2 
 */
function ajouterTerritoireAdj(territoire1, territoire2) {
    territoire1.ajouterTerritoireAdj(territoire2);
    territoire2.ajouterTerritoireAdj(territoire1);
}

/**
 * Donne le territoire associé au polygone
 * @param {Polygone} polygone 
 */
function getTerritoire(polygone) {
    return listeTerritoires[polygone._leaflet_id]
}

/**
 * affiche les informations du territoire sur le jeu
 * @param {territoire} territoire 
 */
function afficherInformation(territoire, event) {
    if (territoire != null) {
        $("#risk__information").css("visibility", "visible");
        $("#risk__information").css({ "top": event.pageY, "left": event.pageX + 20 });
        var information = "Nom du territoire : " + territoire.NomTerritoire + "<br>"
            + "Proprietaire : " + territoire.Joueur.NomJoueur + "<br>"
            + "Nombre d'unité : " + territoire.NombreUnite + "<br>"
            + "Continent : " + territoire.Continent
        $("#risk__information").html(information);
    }
}

function afficherConsole(message) {
    if (message === "clear") {
        $("#risk__console").html('')
    } else {
        $("#risk__console").html($("#risk__console").html() + message)
    }
}

function initialisationTerritoires(territoires) {
    shuffle(territoires)
    var nombreJoueur = getNombreJoueur();
    for (var i = 0; i < territoires.length; i++) {
        territoires[i].Joueur = getJoueur(i % nombreJoueur)
    }
}

/**
 * Mélange un tableau
 * @param {*} a 
 */
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

/**
 * donne le nombre de territoire d'un joueur
 * @param {Joueur} joueur 
 */
function getNombreTerritoire(joueur) {
    var nomJoueur = joueur.NomJoueur
    var nombreTerritoire = 0;
    for (var i = 0; i < listeTerritoires.length; i++) {
        var territoire = listeTerritoires[i]
        if (territoire != undefined) {
            if (territoire.Joueur.NomJoueur === nomJoueur) {
                nombreTerritoire += 1
            }
        }
    }
    return nombreTerritoire
}

/**
 * élimine un joueur de la liste des joueurs
 */
function supprimerJoueur(joueurElimine) {
    delete listeJoueurs[joueurElimine.NomJoueur]
    afficherConsole("<p> Le joueur " + joueurElimine.NomJoueur + " est éliminé </p>")
    if (getNombreJoueur() == 1) {
        afficherConsole("<p> Victoire de " + getJoueur(0).NomJoueur + "</p>")
    }
}

/**
 * afficher le territoire sélectionné pour agir
 */
function afficherTerritoireSelc() {
    if (polygonAttaquant == undefined)
        $("#risk__territoireSelectionner").text("Territoire sélectionne : aucun")
    else
        $("#risk__territoireSelectionner").text("Territoire sélectionne : " + getTerritoire(polygonAttaquant).NomTerritoire)
}

/**
 * donne un bonus de soldats si tout un continent est conquis
 */
function bonusContinents() {
    var nbSoldatsSup = 0
    for (var i = 0; i < continents.length; i++) {
        var bonus = true
        continents[i].forEach(function (territoire) {
            bonus = territoire.Joueur.NomJoueur === getJoueur(JoueurActuel).NomJoueur && bonus
        })
        if (bonus) {
            nbSoldatsSup += bonusSoldats[i]
        }
    }
    return nbSoldatsSup
}